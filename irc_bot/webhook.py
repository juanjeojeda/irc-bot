"""HTTP(S) endpoints."""
import os

from cki_lib import misc
from cki_lib.logger import get_logger
import flask
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

from .queue import IRC_PUBLISH_QUEUE

app = flask.Flask(__name__)

if app.env == 'production':
    sentry_sdk.init(
        ca_certs=os.getenv('REQUESTS_CA_BUNDLE'),
        integrations=[FlaskIntegration()]
    )

LOGGER = get_logger(__name__)


@app.route('/', methods=['GET'])
def index():
    """Return a nearly empty page."""
    return flask.Response(response='OK', status='200')


@app.route('/message', methods=['POST'])
def message_receiver():
    """Route a generic message to IRC."""
    irc_json = flask.request.get_json()
    message = irc_json['message']
    IRC_PUBLISH_QUEUE.put(message)
    return flask.Response(response='OK', status='200')


@app.route('/sentry', methods=['POST'])
def sentry_receiver():
    """Route a Sentry message to IRC."""
    irc_json = flask.request.get_json()
    project_name = irc_json['project_name']
    message = irc_json['message']
    url = misc.shorten_url(irc_json['url'])
    message = f'😩 {project_name}: {message} - {url}'
    IRC_PUBLISH_QUEUE.put(message)
    return flask.Response(response='OK', status='200')


@app.route('/grafana', methods=['POST'])
def grafana_alert_receiver():
    """Route a Grafana alert to IRC."""
    data = flask.request.get_json()
    title = data['title']
    emoji = '🔵' if 'OK' in title else '🔴'
    values = ', '.join(
        f'{item["metric"]}: {item["value"]}'
        for item in data.get('evalMatches', [])
    )
    message = f'{emoji} {title} - {values}'
    IRC_PUBLISH_QUEUE.put(message)
    return flask.Response(response='OK', status='200')
