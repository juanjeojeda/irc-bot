"""Handle IRC commands."""
import datetime
import json
import os
import random
import re

from cki_lib import misc
from cki_lib.gitlab import get_instance
from cki_lib.logger import get_logger
import gitlab
import requests
import yaml

from .cki_jira import Jira
from .irc_message import style

JIRA_SERVER = os.environ['JIRA_SERVER']
JIRA_USERNAME = os.environ['JIRA_USERNAME']
JIRA_PASSWORD = os.environ['JIRA_PASSWORD']
JIRA_CLIENT = Jira(JIRA_SERVER, JIRA_USERNAME, JIRA_PASSWORD)
GITLAB_PRIVATE_TOKEN = os.environ['GITLAB_PRIVATE_TOKEN']
GITLAB_SNIPPET = os.environ['GITLAB_SNIPPET']
OK_EMOJIS = ['👌', '🆗', '🎉', '💃', '😊', '😁', '🥰', '😘', '🤗', '🤭']
TEAM_MEMBERS = os.environ.get('TEAM_MEMBERS', '')
STANDUPS = json.loads(os.environ.get('STANDUPS', '{}'))

LOGGER = get_logger(__name__)


class SearchIssue():
    # pylint: disable=too-few-public-methods
    """Search for a Jira issue."""

    name = 'search'
    usage = ('Search jira issues, e.g. "search text ~ CKI", more info '
             'https://confluence.atlassian.com/jiracoreserver073/'
             'advanced-searching-861257209.html')
    nargs = 'n'

    def run(self, conn, event, args):
        """Process the command."""
        if not args:
            conn.privmsg(event.target, f'{self.usage}')
            return
        pattern = ' '.join(args)
        if 'status' not in pattern:
            pattern += ' AND status!=Done'
        for idx, issue in enumerate(JIRA_CLIENT.search_issues(pattern)):
            conn.privmsg(
                event.target, f' {issue.key} - {issue.fields.summary}')
            if idx >= 5:
                break


class CreateIssueBase():
    # pylint: disable=too-few-public-methods
    """Base class to create a new Jira issue."""

    name = 'create'
    usage = ('Create a jira issue - usage: "new summary. '
             'description: here the description"')
    nargs = 'n'

    def run(self, conn, event, args):
        """Process the command."""
        try:
            summary, description = ' '.join(args).split('description:')
        except ValueError:
            conn.privmsg(
                event.target, f'"description: " is required. {self.usage}')
            return

        nickname = event.source.split('!')[0]
        i_type = 'Bug' if self.name == 'new-bug' else 'Task'
        issue = JIRA_CLIENT.create_issue(summary.strip(), description.strip(),
                                         nickname, i_type=i_type)
        conn.privmsg(
            event.target, f'{random.choice(OK_EMOJIS)} {issue.key} created')


class NewTask(CreateIssueBase):
    # pylint: disable=too-few-public-methods
    """Create a new Jira task."""

    name = 'new-task'


class NewBug(CreateIssueBase):
    # pylint: disable=too-few-public-methods
    """Create a new Jira bug."""

    name = 'new-bug'


class Super():
    # pylint: disable=too-few-public-methods
    """Run multiple commands."""

    name = 'super'
    usage = 'Run a combination of other irc-bot commands delimited by +!'
    nargs = 'n'

    def __init__(self, lst):
        """Construct a new instance."""
        self.lst = lst

    def run(self, conn, event, args):
        """Process the command."""
        if not args:
            conn.privmsg(event.target, f'{self.usage}')
            return

        # arguments are already split, join them to split them
        # in a different manner
        cmd = ' '.join(args)

        # +! delimits each command in command chain
        for cmd_args in [args.split() for args in cmd.split('+!')]:
            for cmd in self.lst:
                if cmd.name == cmd_args[0]:
                    # run one of the commands in chain
                    cmd.run(conn, event, cmd_args)
                    break
            else:
                # one of the commands in cmd_args wasn't found...
                conn.privmsg(event.target, f'Cannot find command'
                                           f' "{cmd_args[0]}". {self.usage}')
                return

        conn.privmsg(
            event.target, f'{random.choice(OK_EMOJIS)} super cmd done!')


class SetState():
    # pylint: disable=too-few-public-methods
    """Transition a Jira issue to a new state."""

    name = 'state'
    usage = 'Set new state to issue - usage: issue In Progress'
    nargs = 'n'

    def run(self, conn, event, args):
        """Process the command."""
        if not args or len(args) < 2:
            conn.privmsg(event.target, f'{self.usage}')
            return
        issue = args[0]
        state = ' '.join(args[1:])
        JIRA_CLIENT.set_state(issue, state)
        conn.privmsg(
            event.target,
            f'{random.choice(OK_EMOJIS)} new state {state} set to {issue}')


class GetBeakerQueues():
    # pylint: disable=too-few-public-methods
    """Output the current status of the Beaker queues."""

    name = 'queues'
    usage = 'Get the current Beaker queues'
    nargs = 0

    @staticmethod
    def _get_queued_jobs(arch, beaker_url, beaker_user):
        # Get the queued jobs for each arch.
        url = f"{beaker_url}/top-recipe-owners.{arch}"
        resp = requests.get(url, stream=True)
        for line in resp.text.split('\n'):
            if line.startswith(beaker_user):
                return line.split()[1]
        return 0

    def run(self, conn, event, _):
        """Process the command."""
        # Get the Beaker URL and error out if it's not set.
        beaker_url = os.environ.get('BEAKER_STATS_URL', None)
        if not beaker_url:
            conn.privmsg(
                event.target,
                '👎 Somebody forgot to set BEAKER_STATS_URL')
            return

        beaker_user = os.environ.get('BEAKER_USER', None)
        if not beaker_user:
            conn.privmsg(
                event.target,
                '👎 Somebody forgot to set BEAKER_USER')
            return

        beaker_host = os.environ.get('BEAKER_HOST', None)
        if not beaker_host:
            conn.privmsg(
                event.target,
                '👎 Somebody forgot to set BEAKER_HOST')
            return

        # Get the total jobs running.
        params = {
            "jobsearch-0.table": "Group",
            "jobsearch-0.operation": "is",
            "jobsearch-0.value": "cki",
            "jobsearch-1.table": "Status",
            "jobsearch-1.operation": "is not",
            "jobsearch-1.value": "Aborted",
            "jobsearch-2.table": "Status",
            "jobsearch-2.operation": "is not",
            "jobsearch-2.value": "Cancelled",
            "jobsearch-3.table": "Status",
            "jobsearch-3.operation": "is not",
            "jobsearch-3.value": "Completed",
        }
        url = f"https://{beaker_host}/jobs/"
        resp = requests.get(url, params=params)
        running_jobs = re.findall(r"Items found: (\d*)", resp.text)[0]

        # Get queues for each architecture.
        arches = ['aarch64', 'ppc64le', 's390x', 's390x_z13', 'x86_64']
        for idx, arch in enumerate(arches):
            result = self._get_queued_jobs(arch, beaker_url, beaker_user)
            arches[idx] = f"{arch}: {result}"

        irc_message = (
            f"🤖 CKI Beaker jobs running: {running_jobs}; "
            f"Queues: {', '.join(arches)}"
        )

        conn.privmsg(event.target, irc_message)


class CommentIssue():
    # pylint: disable=too-few-public-methods
    """Comment on a Jira issue."""

    name = 'comment'
    usage = 'Add a comment on a jira issue - usage: "issue some update"'
    nargs = 'n'

    def run(self, conn, event, args):
        """Process the command."""
        if not args:
            conn.privmsg(event.target, f'{self.usage}')
            return
        issue = args[0]
        message = ' '.join(args[1:])
        if not message:
            conn.privmsg(event.target, f'{self.usage}')
            return
        JIRA_CLIENT.add_comment_to_issue(issue, message)
        conn.privmsg(
            event.target,
            f'{random.choice(OK_EMOJIS)} comment added to {issue}')


class DadJoke():
    # pylint: disable=too-few-public-methods
    """Output a dad joke."""

    name = 'dadjoke'
    usage = 'Get a good Dad joke'
    nargs = 0

    @staticmethod
    def run(conn, event, _):
        """Process the command."""
        headers = {'Accept': 'application/json'}
        resp = requests.get("https://icanhazdadjoke.com/", headers=headers)
        dad_joke = resp.json()['joke'].replace('\n', ' ')
        conn.privmsg(event.target, f'🤠 {dad_joke}')


class SetEstimate():
    # pylint: disable=too-few-public-methods
    """Update the estimate of a Jira issue."""

    name = 'set-estimate'
    usage = 'set estimate for jira issue - usage "estimate" (e.g. 5)'
    nargs = 2

    def run(self, conn, event, args):
        """Process the command."""
        if not args or len(args) < 2:
            conn.privmsg(event.target, f'{self.usage}')
            return

        issue, estimate = args
        emoji = random.choice(OK_EMOJIS)
        JIRA_CLIENT.set_estimate(issue, estimate)
        conn.privmsg(
            event.target,
            f'{emoji} estimate of {estimate} set to issue {issue}')


class AssignIssue():
    # pylint: disable=too-few-public-methods
    """Assign a Jira issue to a user."""

    name = 'assign'
    usage = 're-assign a jira issue - usage: "issue user"'
    nargs = 2

    @staticmethod
    def run(conn, event, args):
        """Process the command."""
        issue, user = args
        emoji = random.choice(OK_EMOJIS)
        JIRA_CLIENT.assign_issue(issue, user)
        conn.privmsg(event.target, f'{emoji} {issue} assigned to {user}')


class ListCommands():
    # pylint: disable=too-few-public-methods
    """List all commands that the bot understands."""

    name = 'list'
    usage = 'list'
    nargs = 0

    @staticmethod
    def run(conn, event, _):
        """Process the command."""
        conn.privmsg(event.target, ', '.join(event.cmd_manager.cmds))


class SuccessBot():
    # pylint: disable=too-few-public-methods
    """Add a success message to the success snippet."""

    name = "success"
    usage = "talk about some success we had!"
    nargs = 'n'

    @staticmethod
    def run(conn, event, args):
        """Process the command."""
        instance = get_instance('https://gitlab.cee.redhat.com',
                                token=GITLAB_PRIVATE_TOKEN)
        # Get the current snippet content.
        snippet = instance.snippets.get(GITLAB_SNIPPET)
        old_content = snippet.content().decode('utf-8')

        # Add new content.
        nickname = event.source.split('!')[0]
        new_success_message = ' '.join(args)
        timestamp = datetime.datetime.now().isoformat(sep=' ',
                                                      timespec='seconds')
        new_content = (
            f"* **{nickname}:** {new_success_message} "
            f"*{timestamp}* \n{old_content}"
        )

        # Update the snippet.
        snippet.content = new_content
        snippet.save()

        # Note the success
        conn.privmsg(
            event.target,
            f"{random.choice(OK_EMOJIS)} success noted! "
            f"https://gitlab.cee.redhat.com/snippets/{GITLAB_SNIPPET}"
        )


class CommandManager():
    """Handle the commands the bot understands."""

    def __init__(self):
        """Create a new instance."""
        self.cmds = {}
        self.register_cmd(ListCommands())

    def register_cmd(self, cmd):
        """Register a new command."""
        self.cmds[cmd.name] = cmd

    def __call__(self, conn, event):
        """Handle a message from IRC."""
        text = event.arguments[0].strip()
        if not text.startswith(conn.ircname):
            return
        args = text.split()

        # Mentioning the bot without any arguments causes it to throw an
        # exception and abruptly quit.
        if len(args) < 2:
            return

        cmd_name = args[1]
        if cmd_name not in self.cmds:
            conn.privmsg(event.target, 'Command not found ¯\\_(ツ)_/¯')
            self.process_command('list', [], conn, event)
            return

        self.process_command(cmd_name, args[2:], conn, event)

    def process_command(self, cmd_name, args, conn, event):
        """Process one command."""
        event.cmd_manager = self
        cmd = self.cmds[cmd_name]
        if cmd.nargs != 'n' and cmd.nargs != len(args):
            conn.privmsg(event.target, f'{cmd.usage}')
            return
        try:
            cmd.run(conn, event, args)
        # pylint: disable=broad-except
        except Exception as exc:
            conn.privmsg(event.target, '😢, ups: {!r}'.format(exc))


class StandUp():
    # pylint: disable=too-few-public-methods
    """Notify the team members of a standup."""

    name = 'standup'
    usage = 'Get standup notification + random order!'
    nargs = 0

    @staticmethod
    def run(conn, event, _):
        """Process the command."""
        members = TEAM_MEMBERS.split()
        random.shuffle(members)
        weekday = str(datetime.datetime.today().weekday())
        title, url = STANDUPS.get(weekday, ['StandUp', None])
        message = f'🙋 {title}: ' + ', '.join(members)
        if url:
            message += f' - {misc.shorten_url(url)}'
        conn.privmsg(event.target, message)


class GitLabMergeRequest():
    """Print information about GitLab merge request URLs."""

    def __call__(self, conn, event):
        """Process an IRC message."""
        text = event.arguments[0].strip()
        pattern = r'(https://[^/]+)/(cki-project/[^ ]*)/merge_requests/(\d+)'
        for match in re.findall(pattern, text):
            try:
                self.process_merge_request(conn, event, *match)
            # pylint: disable=broad-except
            except Exception:
                LOGGER.exception('Unable to process merge request')

        project_pattern = r'([^\s]+)!(\d+)'
        for match in re.findall(project_pattern, text):
            try:
                self.process_merge_request(conn, event, None, *match)
            # pylint: disable=broad-except
            except Exception:
                LOGGER.exception('Unable to process merge request')

    @staticmethod
    def _search_gitlab_project(project, merge_request):
        """Search through known Gitlab instances for the project."""
        if '/' not in project:
            project = f'cki-project/{project}'
        results = []

        hosts = yaml.safe_load(os.environ.get('GITLAB_TOKENS', '{}')).keys()
        for host in hosts:
            gl_instance = get_instance(f'https://{host}')
            try:
                gl_project = gl_instance.projects.get(project)
                if gl_project.archived:
                    continue
                gl_mr = gl_project.mergerequests.get(int(merge_request))
                results.append((gl_project, gl_mr))
            except gitlab.GitlabGetError:
                pass

        if len(results) > 1:
            open_mrs = [r for r in results if r[1].state == 'opened']
            if len(open_mrs) == 1:
                return open_mrs

        return results

    @staticmethod
    def process_merge_request(conn, event, instance_url, project,
                              merge_request):
        """Print information about a GitLab merge request."""
        LOGGER.info('Processing GitLab MR %s %s %s',
                    instance_url, project, merge_request)
        if instance_url:
            gl_instance = get_instance(instance_url)
            gl_project = gl_instance.projects.get(re.sub('/-$', '', project))
            results = [(
                gl_project, gl_project.mergerequests.get(int(merge_request))
            )]
        else:
            results = GitLabMergeRequest._search_gitlab_project(project,
                                                                merge_request)

        for gl_project, gl_mr in results:
            msg = GitLabMergeRequest.generate_message(gl_project, gl_mr)
            conn.privmsg(event.target, msg)

    @staticmethod
    def get_ci_status(gl_mr):
        """Get the status of the last pipeline on the MR."""
        status = 'UNKN'
        pipeline_status = {
            'canceled': style('CANC', fg='red'),
            'failed': style('FAIL', fg='red'),
            'pending': 'PEND',
            'running': 'RUNN',
            'success': style('PASS', fg='lime'),
        }

        pipelines = gl_mr.pipelines(per_page=1)
        if pipelines:
            last_pipeline = pipelines[0]
            status = pipeline_status.get(last_pipeline['status'], status)
        return f'[{gl_mr.state.upper()}/{status}]'

    @staticmethod
    def get_file_stats(gl_mr):
        """Get statistics about file changes."""
        changes = gl_mr.changes()
        files = changes['changes_count']
        added = sum(len(re.findall(r'^\+', c['diff'], re.M))
                    for c in changes['changes'])
        removed = sum(len(re.findall('^-', c['diff'], re.M))
                      for c in changes['changes'])
        added = style(f'+{added}', fg='lime')
        removed = style(f'-{removed}', fg='red')
        return f'{files}/{added}/{removed}'

    @staticmethod
    def generate_message(gl_project, gl_mr):
        """Generate message to reply."""
        details = (
            style(f'{gl_project.path}!{gl_mr.iid}', bold=True),
            gl_mr.author["username"],
            GitLabMergeRequest.get_file_stats(gl_mr),
            GitLabMergeRequest.get_ci_status(gl_mr),
            gl_mr.title,
        )
        url = misc.shorten_url(gl_mr.web_url)

        return f'{" ".join(details)} - {url}'
