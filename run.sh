#!/bin/bash

export START_FLASK=irc_bot.webhook
export START_PYTHON_IRC=irc_bot.irc
export START_PYTHON_AMQP=irc_bot.amqp
export START_REDIS=true
export LOG_NAME=irc-bot

exec cki_entrypoint.sh
