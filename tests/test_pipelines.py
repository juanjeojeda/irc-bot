"""Pipeline handling tests."""
import unittest
from unittest import mock

from irc_bot import amqp


@mock.patch.dict('os.environ', {'SHORTENER_URL': ''})
@mock.patch('irc_bot.amqp.IRC_PUBLISH_QUEUE', mock.Mock())
class TestPipeline(unittest.TestCase):
    """ Test webhooks."""

    def test_no_jobs_retrigger(self):
        """Check that retriggered empty pipelines are handled correctly."""
        payload = {
            "object_kind": "pipeline",
            "object_attributes": {
                "id": 612710, "stages": [], "status": "failed", "variables": []
            },
            "project": {"web_url": "url"},
            "commit": {
                "message": "Retrigger: retrigger-brew\n"
                "cki_pipeline_type = retrigger-brew\n"
                "title = Retrigger: retrigger-brew\n"
                "subject = Retrigger: Brew: Task 46865701\n"
                "retrigger = true",
            },
        }
        result = amqp.process_message('gitlab.project.pipeline', payload)
        self.assertEqual(result, [])

    def test_no_jobs(self):
        """Check that pipelines without jobs are handled correctly."""
        payload = {
            "object_kind": "pipeline",
            "object_attributes": {
                "id": 612710, "stages": [], "status": "failed", "variables": []
            },
            "project": {"web_url": "url"},
            "commit": {
                "message": "Message\n"
                "cki_pipeline_type = brew\n"
                "title = Title\n"
                "subject = Brew: Task 46865701\n"
            },
        }
        result = amqp.process_message('gitlab.project.pipeline', payload)
        self.assertEqual(len(result), 1)

    def test_sentry(self):
        """Check that sentry alerts are handled correctly."""
        payload = {
            'action': 'triggered',
            'data': {'event': {
                'title': 'title',
                'url': 'https://sentry.io/api/0/projects/o/project/events/1/',
                'web_url': 'https://web_url',
            }}
        }
        result = amqp.process_message(
            'sentry.project.event_alert.triggered', payload)
        self.assertEqual(len(result), 1)
        self.assertIn('title', result[0])
        self.assertIn('project', result[0])
        self.assertIn('web_url', result[0])

    def test_sentry_ignored(self):
        """Check that sentry issues are ignored."""
        payload = {
            'action': 'created',
            'data': {'issue': {}}
        }
        result = amqp.process_message(
            'sentry.project.issue.created', payload)
        self.assertEqual(len(result), 0)
