"""Webhook interaction tests."""
import unittest
from unittest import mock

from irc_bot import webhook


class TestWebhook(unittest.TestCase):
    """ Test webhooks."""

    def setUp(self):
        """SetUp class."""
        self.client = webhook.app.test_client()

    def test_index(self):
        """Check the index page."""
        response = self.client.get('/')
        self.assertEqual(response.status, '200')

    @mock.patch('irc_bot.webhook.IRC_PUBLISH_QUEUE')
    def test_message(self, queue):
        """Check routing of a message."""
        response = self.client.post(
            '/message', json={'message': 'content'})
        self.assertEqual(response.status, '200')
        queue.put.assert_called_with('content')

    @mock.patch('irc_bot.webhook.IRC_PUBLISH_QUEUE')
    @mock.patch.dict('os.environ', {'SHORTENER_URL': ''})
    def test_sentry(self, queue):
        """Check routing of a Sentry message."""
        response = self.client.post(
            '/sentry', json={'project_name': 'project',
                             'message': 'content',
                             'url': 'url'})
        self.assertEqual(response.status, '200')
        queue.put.assert_called()
        self.assertIn('project', queue.put.mock_calls[0].args[0])
        self.assertIn('content', queue.put.mock_calls[0].args[0])
        self.assertIn('url', queue.put.mock_calls[0].args[0])

    @mock.patch('irc_bot.webhook.IRC_PUBLISH_QUEUE')
    @mock.patch.dict('os.environ', {'SHORTENER_URL': ''})
    def test_grafana(self, queue):
        """Check routing of a Grafana alert."""
        response = self.client.post(
            '/grafana', json={
                'title': 'content',
                'evalMatches': [{'metric': 'name', 'value': 'num'}],
            })
        self.assertEqual(response.status, '200')
        queue.put.assert_called()
        self.assertIn('content', queue.put.mock_calls[0].args[0])
        self.assertIn('name', queue.put.mock_calls[0].args[0])
        self.assertIn('num', queue.put.mock_calls[0].args[0])
